#!/bin/bash

if [ $EUID -ne 0 ]
then
	/usr/bin/echo "Cannot create symlink without root."
	/usr/bin/echo "Please try again as root."
else
	/usr/bin/chmod +x $PWD/ntlm_tools.py
	/usr/bin/ln -s $PWD/ntlm_tools.py /usr/local/sbin/ntlm-tools
fi