#!/usr/bin/env python3

import getpass
import hashlib
import binascii
from sys import argv


class NTLM:
	def generate(self, ctext):
		enc_hash = hashlib.new('md4', ctext.encode('utf-16le')).digest()
		hash = binascii.hexlify(enc_hash).decode()
		return hash

	def save(self, hash_list):
		with open('hash.txt', 'w+') as temp:
			for hash in hash_list:
				temp.write("{}\n".format(hash.rstrip()))

	def read(self, file):
		cpwds = []
		with open(file, 'r') as pwords:
			for pword in pwords:
				cpwds.append(pword.rstrip())
		return cpwds

	def verify(self, hash):
		valid_chars = ["a", "b", "c", "d", "e", "f"]
		for num in range(0,10):
			valid_chars.append(str(num))
		if len(hash) != 32:
			return False
		for char in hash:
			if char.lower() not in valid_chars:
				return False
		return True


def main():
	ntlm = NTLM()
	choices = ["read", "verify", "generate", "help"]
	hash_list = []
	try:
		if argv[1].lower() == "read":
			passwd_list = ntlm.read(argv[2])
			for word in passwd_list:
				hash_list.append("{} {}".format(ntlm.generate(word.rstrip()), word.rstrip()))
				ntlm.save(hash_list)
		elif argv[1].lower() == "verify":
			if ntlm.verify(argv[2]):
				print("The hash is valid.")
			else:
				print("This does not appear to be a valid MD4 hash.")
		elif argv[1].lower() == "generate":
			try:
				ctext = argv[2]
			except IndexError:
				cont = False
				while not cont:
					ctext = getpass.getpass(prompt="Enter Password: ")
					vtext = getpass.getpass(prompt="Verify Password: ")
					if ctext != vtext:
						print("\nPasswords do not match. Please try again.")
					else:
						cont = True
			hash = ntlm.generate(ctext)
			hash_list.append(hash)
			print("\n{}\n".format(hash))
			choice = input("Save to hash.txt?\n[y/n] > ") or "no"
			if choice.lower() in ("yes", "y"):
				ntlm.save(hash_list)
	except IndexError:
		print("ERROR: Missing argument")
		print("To use, please use one of the following modules:")
		print("{} generate [password value]".format(argv[0]))
		print("{} verify [hash value]".format(argv[0]))
		print("{} read [file name]".format(argv[0]))


if __name__ == '__main__':
	try:
		main()
	except KeyboardInterrupt:
		print()